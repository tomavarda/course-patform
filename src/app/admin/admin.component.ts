import { Component } from '@angular/core';
import { sidenavMenu } from '@core/components/sidenav/sidenav.config';
import { UserRole } from '@auth/interfaces/auth';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent {
  menuItems = sidenavMenu[UserRole.ADMIN];
}
