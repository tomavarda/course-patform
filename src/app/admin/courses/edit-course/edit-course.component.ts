import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { NgClass, NgForOf, NgIf } from '@angular/common';

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ReactiveFormsModule, NgForOf, NgIf, NgClass],
})
export class EditCourseComponent {
  lessonForm: FormGroup;
  activeIndex: number = -1;

  constructor(private formBuilder: FormBuilder) {
    this.lessonForm = this.formBuilder.group({
      lessons: this.formBuilder.array([]),
    });
  }

  get lessonControls() {
    return (this.lessonForm.get('lessons') as FormArray).controls;
  }

  addLesson() {
    const lessonGroup = this.formBuilder.group({
      title: '',
      videoCover: '',
    });
    (this.lessonForm.get('lessons') as FormArray).push(lessonGroup);
    this.setActiveLesson(this.lessonControls.length - 1);
  }

  removeLesson(index: number) {
    (this.lessonForm.get('lessons') as FormArray).removeAt(index);
    if (this.activeIndex === index) {
      this.setActiveLesson(-1);
    } else if (this.activeIndex > index) {
      this.setActiveLesson(this.activeIndex - 1);
    }
  }

  setActiveLesson(index: number) {
    this.activeIndex = index;
  }
}
