import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormArray, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { CourseFormService } from '@admin/courses/add-course/course-form.service';
import { NgForOf, NgIf } from '@angular/common';
import { SafeHtmlPipe } from '@shared/pipes/safe-html/safe-html.pipe';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';

@Component({
  selector: 'app-lessons-step',
  templateUrl: './lessons-step.component.html',
  styleUrls: ['./lessons-step.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    ReactiveFormsModule,
    NgForOf,
    SafeHtmlPipe,
    MatInputModule,
    NgIf,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
  ],
})
export class LessonsStepComponent {
  activeIndex: number = 0;

  readonly lessonsForm = this.courseFormService.lessonsFormGroup;

  constructor(private courseFormService: CourseFormService) {
    this.courseFormService.addNewLesson();
  }

  get lessonsFormArray(): FormArray {
    return this.courseFormService.lessonsFormArray;
  }

  addNewLesson(): void {
    if (!this.lessonsFormArray.at(this.activeIndex)?.valid) {
      return;
    }

    this.courseFormService.addNewLesson();

    this.setActiveLesson(this.lessonsFormArray.controls.length - 1);
  }

  removeLesson(index: number) {
    this.courseFormService.removeLesson(index);
    this.updateActiveLesson(index);
  }

  setActiveLesson(index: number) {
    this.activeIndex = index;
  }

  private updateActiveLesson(index: number): void {
    if (this.activeIndex === index) {
      this.setActiveLesson(0);
    } else if (this.activeIndex > index) {
      this.setActiveLesson(this.activeIndex - 1);
    }
  }
}
