import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { CourseFormService } from '@admin/courses/add-course/course-form.service';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatStepperModule } from '@angular/material/stepper';

@Component({
  selector: 'app-common-info-step',
  templateUrl: './common-info-step.component.html',
  styleUrls: ['./common-info-step.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ReactiveFormsModule, MatInputModule, MatButtonModule, MatStepperModule],
})
export class CommonInfoStepComponent {
  readonly titleFormGroup = this.courseFormService.titleFormGroup;

  constructor(private courseFormService: CourseFormService) {}
}
