import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class CourseFormService {
  readonly titleFormGroup: FormGroup;
  readonly lessonsFormGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.lessonsFormGroup = this.buildLessonsForm();
    this.titleFormGroup = this.buildTitleForm();
  }

  get lessonsFormArray(): FormArray {
    return this.lessonsFormGroup.get('lessons') as FormArray;
  }

  addNewLesson(): void {
    const preparedLessonGroup = this.prepareNewLessonForm();

    this.lessonsFormArray.push(preparedLessonGroup, { emitEvent: false });
  }

  removeLesson(index: number): void {
    this.lessonsFormArray.removeAt(index, { emitEvent: false });
  }

  resetLessonForm(): void {
    while (this.lessonsFormArray.length !== 0) {
      this.lessonsFormArray.removeAt(0);
    }
  }

  private buildLessonsForm(): FormGroup {
    return this.fb.group({
      lessons: this.fb.array([]),
    });
  }

  private prepareNewLessonForm(): FormGroup {
    return this.fb.group({
      title: ['', Validators.required],
      videoUrl: ['', Validators.required],
      description: [''],
      additionalMaterials: [''],
    });
  }

  private buildTitleForm(): FormGroup {
    return this.fb.group({
      title: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(250)]],
    });
  }
}
