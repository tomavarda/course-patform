import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatStepperModule } from '@angular/material/stepper';
import { FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { JsonPipe, NgClass, NgForOf, NgIf } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { SafeHtmlPipe } from '@shared/pipes/safe-html/safe-html.pipe';
import { CourseFormService } from '@admin/courses/add-course/course-form.service';
import { LessonsStepComponent } from '@admin/courses/add-course/lessons-step/lessons-step.component';
import { CommonInfoStepComponent } from '@admin/courses/add-course/common-info-step/common-info-step.component';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    MatStepperModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    NgForOf,
    NgIf,
    NgClass,
    JsonPipe,
    SafeHtmlPipe,
    LessonsStepComponent,
    CommonInfoStepComponent,
    MatListModule,
    MatCardModule,
  ],
})
export class AddCourseComponent {
  readonly titleFormGroup = this.courseFormService.titleFormGroup;
  readonly lessonsForm = this.courseFormService.lessonsFormGroup;

  constructor(private courseFormService: CourseFormService) {}

  get lessonsFormArray(): FormArray {
    return this.courseFormService.lessonsFormArray;
  }

  publishCourse() {}
}
