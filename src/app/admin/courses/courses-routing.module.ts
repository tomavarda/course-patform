import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CoursesComponent } from '@admin/courses/courses.component';
import { AddCourseComponent } from '@admin/courses/add-course/add-course.component';
import { EditCourseComponent } from '@admin/courses/edit-course/edit-course.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesComponent,
    children: [
      {
        path: 'add-course',
        component: AddCourseComponent,
      },
      {
        path: 'edit-course',
        component: EditCourseComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoursesRoutingModule {}
