import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from '@admin/admin.component';
import { AdminRoutingModule } from '@admin/admin-routing.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { CoursesComponent } from '@admin/courses/courses.component';
import { SidenavComponent } from '@core/components/sidenav/sidenav.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpsInterceptor } from '@core/interceptors/http.interceptor';

@NgModule({
  declarations: [AdminComponent, CoursesComponent],
  imports: [CommonModule, AdminRoutingModule, MatButtonModule, MatIconModule, MatSidenavModule, SidenavComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsInterceptor,
      multi: true,
    },
  ],
})
export class AdminModule {}
