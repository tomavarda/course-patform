import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { ConsumerComponent } from '@consumer/consumer.component';
import { ConsumerRoutingModule } from '@consumer/consumer-routing.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { HeaderComponent } from '@core/components/header/header.component';
import { SidenavComponent } from '@core/components/sidenav/sidenav.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpsInterceptor } from '@core/interceptors/http.interceptor';

@NgModule({
  declarations: [ConsumerComponent],
  imports: [
    CommonModule,
    ConsumerRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    NgOptimizedImage,
    HeaderComponent,
    SidenavComponent,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsInterceptor,
      multi: true,
    },
  ],
})
export class ConsumerModule {}
