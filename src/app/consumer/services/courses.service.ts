import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, of } from 'rxjs';
import { Course } from '@consumer/interfaces/courses';

@Injectable({
  providedIn: 'root',
})
export class CoursesService {
  constructor(private http: HttpClient) {}

  getCoursesList(): Observable<Course[]> {
    return this.http.get<Course[]>(`https://courses-2023-default-rtdb.firebaseio.com/coourses.json`);
    // return of(null).pipe(
    //   map(() => {
    //     return [
    //       {
    //         title: 'Course 1',
    //         id: 12,
    //         coverImgSrc:
    //           'https://images.adsttc.com/media/images/6308/2af8/fa26/793f/ed20/7aab/large_jpg/cilada-verde-potenciais-problemas-no-projeto-paisagistico_1.jpg?1661479681',
    //         lessons: [
    //           {
    //             title: 'Lesson 1',
    //             videoSrc: 'https://youtu.be/UC2TRPrbmyI',
    //           },
    //         ],
    //       },
    //       {
    //         title: 'Course 2',
    //         id: 23,
    //         coverImgSrc: 'https://evergreenlandscapes.ca/wp-content/uploads/2018/06/grass.jpg.webp',
    //         lessons: [
    //           {
    //             title: 'Lesson 2.1',
    //             videoSrc: 'https://youtu.be/UC2TRPrbmyI',
    //           },
    //         ],
    //       },
    //     ];
    //   })
    // );
  }
}
