import { ChangeDetectionStrategy, Component } from '@angular/core';
import { sidenavMenu } from '@core/components/sidenav/sidenav.config';
import { UserRole } from '@auth/interfaces/auth';

@Component({
  selector: 'app-consumer',
  templateUrl: './consumer.component.html',
  styleUrls: ['./consumer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConsumerComponent {
  menuItems = sidenavMenu[UserRole.CONSUMER];
}
