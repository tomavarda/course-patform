import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoursesComponent } from '@consumer/courses/courses.component';
import { CourseLessonComponent } from '@consumer/courses/course-lesson/course-lesson.component';
import { RouterLink } from '@angular/router';

@NgModule({
  declarations: [CoursesComponent, CourseLessonComponent],
  imports: [CommonModule, RouterLink],
})
export class CoursesModule {}
