import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CoursesService } from '../services/courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CoursesComponent {
  coursesList$ = this.coursesService.getCoursesList();

  constructor(private coursesService: CoursesService) {}
}
