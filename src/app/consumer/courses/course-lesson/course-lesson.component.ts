import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-course-lesson',
  templateUrl: './course-lesson.component.html',
  styleUrls: ['./course-lesson.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CourseLessonComponent {
  videoSource: string = 'https://youtu.be/UC2TRPrbmyI';

  constructor(private domSanitizer: DomSanitizer) {}
}
