export interface Course {
  id: number;
  title: string;
  coverImgSrc: string;
  lessons: Lesson[];
}

export interface Lesson {
  title: string;
  videoSrc: string;
}
