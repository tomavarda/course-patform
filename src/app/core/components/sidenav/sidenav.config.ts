import { UserRole } from '@auth/interfaces/auth';

export const sidenavMenu = {
  [UserRole.ADMIN]: [
    { routerLink: 'overview', icon: 'dashboard', title: 'Overview' },
    { routerLink: 'courses', icon: 'label_important', title: 'Courses' },
  ],
  [UserRole.CONSUMER]: [
    { routerLink: 'dashboard', icon: 'dashboard', title: 'Dashboard' },
    { routerLink: 'courses', icon: 'label_important', title: 'Courses' },
  ],
};
