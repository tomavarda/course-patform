import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { AuthService } from '@auth/services/auth.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { NgForOf } from '@angular/common';

@UntilDestroy()
@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MatSidenavModule, RouterLinkActive, NgForOf, MatIconModule, MatButtonModule, RouterOutlet, RouterLink],
})
export class SidenavComponent {
  @Input() menuItems: any;

  constructor(private authService: AuthService, private router: Router) {}

  signOut() {
    this.authService
      .signOut()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.router.navigate(['auth', 'login']);
      });
  }
}
