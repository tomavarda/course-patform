import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { FieldErrorComponent } from '../../../shared/component/field-error/field-error.component';
import { UserService } from '../../services/user.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AsyncPipe, NgIf } from '@angular/common';
import { FileUploaderComponent } from '../../../shared/component/file-uploader/file-uploader.component';
import { LetDirective } from '../../../shared/directives/let.directive';
import { MatButtonModule } from '@angular/material/button';

@UntilDestroy()
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    ReactiveFormsModule,
    MatInputModule,
    FieldErrorComponent,
    AsyncPipe,
    FileUploaderComponent,
    NgIf,
    LetDirective,
    MatButtonModule,
  ],
})
export class UserProfileComponent implements OnInit {
  isEditing = false;

  readonly userForm: FormGroup;
  readonly user$ = this.userService.user$;

  constructor(private fb: FormBuilder, private userService: UserService) {
    this.userForm = this.buildForm();
  }

  ngOnInit() {
    this.user$.pipe(untilDestroyed(this)).subscribe((user) => {
      console.log(user);
      this.userForm.patchValue(user);
    });
  }

  save() {
    console.log(this.userForm);
    if (this.userForm.value && this.userForm.touched) {
      return;
    }
    console.log(this.userForm.value);

    this.userService
      .updateUser(this.userForm.value)
      .pipe(untilDestroyed(this))
      .subscribe((value) => {
        console.log(value);

        this.changeEditingMode();
      });
  }

  changeEditingMode(): void {
    this.isEditing = !this.isEditing;
  }

  private buildForm(): FormGroup {
    return this.fb.group({
      displayName: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.minLength(6)],
    });
  }
}
