import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterLink } from '@angular/router';
import { AsyncPipe, JsonPipe } from '@angular/common';
import { GetUserInitialsPipe } from '../../../shared/pipes/get-user-initials/get-user-initials.pipe';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MatIconModule, AsyncPipe, JsonPipe, RouterLink, GetUserInitialsPipe],
})
export class HeaderComponent {
  user$ = this.userService.user$;

  constructor(private userService: UserService) {}
}
