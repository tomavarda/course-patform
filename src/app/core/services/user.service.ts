import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorageService } from './local-storage.service';
import { User } from '@auth/interfaces/auth';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private path = environment.urlDefault;

  private readonly _user = new BehaviorSubject<User | null>(this.storeService.getItem('user'));

  constructor(private storeService: LocalStorageService, private http: HttpClient) {}

  get user$(): Observable<User | null> {
    return this._user.asObservable();
  }

  setUser(user: User | null): void {
    this.storeService.setItem('user', user);

    this._user.next(user);
  }

  updateUser(userData: Partial<User>): Observable<any> {
    const { id: userId, ...user } = userData;

    this.setUser({ ...this._user.value, ...userData });

    return this.http.put(`${this.path}/user/update/${userId}`, user);
  }

  getCurrentUser(token: string): Observable<User> {
    return this.http.get<User>(`${this.path}/user/current`, { headers: { Authorization: token } });
  }

  // setAdminStatus(user: FBUser): Observable<any> {
  //   return this.http.post(`https://courses-2023-default-rtdb.firebaseio.com/users.json/${user.uid}`, user);
  // }
}
