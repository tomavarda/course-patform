import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error) => {
        console.log(error);
        this.snackBar.open(error.message);

        switch (error.code) {
          case 401: {
            this.snackBar.open('Your session has expired. Please relogin');

            return throwError(error);
          }

          case 403: {
            this.snackBar.open("You don't have authorisation to perform this action");

            return throwError(error);
          }

          default: {
            console.log(error);
            this.snackBar.open(error.error.message);

            return throwError(error);
          }
        }
      })
    );
  }
}
