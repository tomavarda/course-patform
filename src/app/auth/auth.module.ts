import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from '@auth/auth.component';
import { AuthRoutingModule } from '@auth/auth-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { GetInputErrorPipe } from '@shared/pipes/get-input-error/get-input-error.pipe';
import { FieldErrorComponent } from '@shared/component/field-error/field-error.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [AuthComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    ReactiveFormsModule,
    MatInputModule,
    GetInputErrorPipe,
    FieldErrorComponent,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [AuthComponent],
})
export class AuthModule {}
