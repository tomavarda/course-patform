export interface User {
  username: string;
  email: string;
  role: UserRole;
  id: string;
}

export interface UserCredential {
  username: string;
  password: string;
}

export enum UserRole {
  ADMIN = 'ADMIN',
  CONSUMER = 'CONSUMER',
}

export interface LoginResponse {
  token: string;
  expiresIn: string;
}

// Student Contacts:
//
//   info@enginprogram.org
//
// https://www.enginprogram.org
//
// Social media:
//
//   https://www.instagram.com/enginprogram/
//
//     https://www.facebook.com/enginprogram/
