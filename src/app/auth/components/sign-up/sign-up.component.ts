import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FieldErrorComponent } from '@shared/component/field-error/field-error.component';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FormUtils } from '@shared/utils/form-utils';
import { AuthService } from '../../services/auth.service';
import { Router, RouterLink } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { MatRadioModule } from '@angular/material/radio';
import { UserRole } from '@auth/interfaces/auth';
import { NgForOf, TitleCasePipe } from '@angular/common';

@UntilDestroy()
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    FieldErrorComponent,
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    RouterLink,
    MatRadioModule,
    NgForOf,
    TitleCasePipe,
  ],
})
export class SignUpComponent {
  isHiddenPassword = true;
  isHiddenConfirmPassword = true;

  readonly userRoles: UserRole[] = [UserRole.ADMIN, UserRole.CONSUMER];
  readonly signUpForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.signUpForm = this.buildForm();
  }

  signUp(): void {
    if (this.signUpForm.invalid) {
      return;
    }

    this.authService
      .signUp(this.signUpForm.value)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.router.navigate(['login']);
      });
  }

  private buildForm(): FormGroup {
    return this.fb.group(
      {
        username: ['', [Validators.required, Validators.minLength(2)]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', FormUtils.passwordValidators()],
        confirmPassword: ['', FormUtils.passwordValidators()],
        role: [UserRole.CONSUMER, Validators.required],
      },
      {
        validators: FormUtils.matchFields('password', 'confirmPassword'),
      }
    );
  }
}
