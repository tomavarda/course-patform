import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { FieldErrorComponent } from '@shared/component/field-error/field-error.component';
import { MatIconModule } from '@angular/material/icon';
import { FormUtils } from '@shared/utils/form-utils';
import { NgIf } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AuthService } from '../../services/auth.service';
import { Router, RouterLink } from '@angular/router';
import { UserRole } from '@auth/interfaces/auth';

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [MatInputModule, MatButtonModule, ReactiveFormsModule, FieldErrorComponent, MatIconModule, NgIf, RouterLink],
})
export class LoginComponent {
  isHidden = true;

  readonly loginForm: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
    this.loginForm = this.buildForm();
  }

  login(): void {
    if (this.loginForm.invalid) {
      return;
    }

    this.authService
      .login(this.loginForm.value)
      .pipe(untilDestroyed(this))
      .subscribe((user) => {
        this.router.navigate([user.role.toLowerCase()]);
      });
  }

  private buildForm(): FormGroup {
    return this.fb.group({
      username: ['', [Validators.required, Validators.minLength(2)]],
      password: ['', FormUtils.passwordValidators()],
    });
  }
}
