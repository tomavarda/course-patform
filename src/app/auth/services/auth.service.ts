import { Injectable } from '@angular/core';
import { from, map, Observable, of, switchMap, tap } from 'rxjs';
import { LoginResponse, User, UserCredential } from '@auth/interfaces/auth';
import { UserService } from '@core/services/user.service';
import { LocalStorageService } from '@core/services/local-storage.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private path = environment.urlDefault;

  constructor(
    private userService: UserService,
    private storeService: LocalStorageService,
    private router: Router,
    private http: HttpClient
  ) {
    // this.afAuth.authState.subscribe((user) => {
    //   if (user) {
    //     user.getIdTokenResult().then((tokenResult) => {
    //       if (Date.parse(tokenResult.expirationTime) <= Date.now()) {
    //         this.refreshToken().subscribe(
    //           (refreshedToken) => {
    //             this.storeService.setItem('refreshToken', refreshedToken);
    //           },
    //           (error) => {
    //             this.router.navigate(['/login']);
    //           }
    //         );
    //       }
    //     });
    //   } else {
    //     this.router.navigate(['/login']);
    //   }
    // });
  }

  get isLoggedIn(): boolean {
    return !!this.token;
  }

  get token(): string {
    return this.storeService.getItem('token');
  }

  signUp({ email, password, username, role }: UserCredential & User): Observable<User> {
    return this.http.post<{ userId: string }>(`${this.path}/auth/sign-in`, { username, password }).pipe(
      switchMap(({ userId: id }) => this.userService.updateUser({ email, role, id })),
      tap((user) => {
        this.storeService.setItem('user', user);
      })
    );
  }

  login({ username, password }: UserCredential): Observable<User> {
    return this.http.post<LoginResponse>(`${this.path}/auth/login`, { username, password }).pipe(
      switchMap(({ token, expiresIn }) => {
        this.storeService.setItem('token', token);

        return this.userService.getCurrentUser(token);
      })
    );
  }

  signOut(): Observable<void> {
    return this.http.get<void>(`${this.path}/auth/logout`);
  }

  private refreshToken(): Observable<any> {
    return this.http.get(`${this.path}/auth/refreshToken`).pipe(tap(() => {}));
  }
}
