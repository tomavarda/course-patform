import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionsCardComponent } from './component/actions-card/actions-card.component';

@NgModule({
  declarations: [
    ActionsCardComponent
  ],
  imports: [CommonModule],
})
export class SharedModule {}
