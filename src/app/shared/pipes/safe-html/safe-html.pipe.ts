import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Pipe({
  name: 'safeHtml',
  standalone: true,
})
export class SafeHtmlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(html: string): SafeUrl {
    console.log(this.sanitizer.bypassSecurityTrustResourceUrl(html));
    return this.sanitizer.bypassSecurityTrustResourceUrl(html);
  }
}
