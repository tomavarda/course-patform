import { Pipe, PipeTransform } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Pipe({
  name: 'getInputError',
  standalone: true,
})
export class GetInputErrorPipe implements PipeTransform {
  transform(error: ValidationErrors | null | undefined): string {
    if (!error) {
      return '';
    }
    console.log(error);

    return Object.values(error)[0];
  }
}
