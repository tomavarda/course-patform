import { Pipe, PipeTransform } from '@angular/core';
import firebase from 'firebase/compat';
import User = firebase.User;

@Pipe({
  name: 'getUserInitials',
  standalone: true,
})
export class GetUserInitialsPipe implements PipeTransform {
  transform(name: User['displayName']): string {
    if (!name) {
      return '';
    }

    return name.slice(0, 1);
  }
}
