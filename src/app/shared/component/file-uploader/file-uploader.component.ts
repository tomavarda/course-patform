import { ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { UploadService } from '../../services/upload.service';
import { MatIconModule } from '@angular/material/icon';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-file-uploader',
  templateUrl: './file-uploader.component.html',
  styleUrls: ['./file-uploader.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [ReactiveFormsModule, MatIconModule],
})
export class FileUploaderComponent implements OnInit {
  @ViewChild('uploadInput') uploadInput: ElementRef;

  fileControl = new FormControl();

  constructor(private uploadService: UploadService) {}

  ngOnInit() {
    this.fileControl.valueChanges
      .pipe(
        switchMap((path) => {
          const file = this.uploadInput.nativeElement.files[0];

          let formData = new FormData();
          formData.set('file', file);

          console.log(file, formData);
          return this.uploadService.uploadFile(path, formData);
        })
      )
      .subscribe();
  }

  uploadAttachment(files: any) {
    console.log(files);
  }

  uploadFile() {}
}
