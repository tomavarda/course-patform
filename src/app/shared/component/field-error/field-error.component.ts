import { ChangeDetectionStrategy, Component, Inject, Input, Optional, Self } from '@angular/core';
import { EMPTY, merge, Observable, of } from 'rxjs';
import {
  AbstractControl,
  FormArrayName,
  FormGroupDirective,
  FormGroupName,
  NgControl,
  ValidationErrors,
} from '@angular/forms';
import { AsyncPipe, NgIf } from '@angular/common';
import { VALIDATION_ERRORS } from '../../tokens/validation-errors';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-field-error',
  templateUrl: './field-error.component.html',
  styleUrls: ['./field-error.component.scss'],
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [AsyncPipe, NgIf, MatIconModule],
})
export class FieldErrorComponent {
  @Input() force: boolean | null = null;

  constructor(
    @Optional()
    @Self()
    @Inject(NgControl)
    private readonly ngControl: NgControl | null,
    @Optional()
    @Self()
    @Inject(FormArrayName)
    private readonly formArrayName: FormArrayName | null,
    @Optional()
    @Self()
    @Inject(FormGroupDirective)
    private readonly formGroup: FormGroupDirective | null,
    @Optional()
    @Self()
    @Inject(FormGroupName)
    private readonly formGroupName: FormGroupName | null,
    @Inject(VALIDATION_ERRORS)
    private readonly validationErrors: ValidationErrors
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  get change$(): Observable<unknown> {
    return merge(this.control?.valueChanges || EMPTY, this.control?.statusChanges || EMPTY);
  }

  get forced(): boolean {
    return typeof this.force === 'boolean' && this.force;
  }

  registerOnChange(): void {}

  registerOnTouched(): void {}

  setDisabledState(): void {}

  writeValue(): void {}

  private get invalid(): boolean {
    return !!this.control && this.control.invalid;
  }

  private get touched(): boolean {
    return !!this.control && this.control.touched;
  }

  get computedError(): Observable<string | null> {
    if (this.forced) {
      return (this.invalid && this.error) || of(null);
    }

    return (this.invalid && this.touched && this.error) || of(null);
  }

  private get control(): AbstractControl | null {
    if (this.ngControl) {
      return this.ngControl.control;
    }

    if (this.formArrayName) {
      return this.formArrayName.control;
    }

    if (this.formGroupName) {
      return this.formGroupName.control;
    }

    if (this.formGroup) {
      return this.formGroup.control;
    }

    return null;
  }

  private get controlErrors(): Record<string, unknown> {
    return this.control?.errors || {};
  }

  private getError(firstError: string): Observable<string> {
    return of(firstError);
  }

  private get error(): Observable<string | null> {
    const [errorName, errorValue] = Object.entries(this.controlErrors)[0];

    if (!errorName || !errorValue) {
      return of(null);
    }

    return this.getError(this.validationErrors[errorName](errorValue));
  }
}
