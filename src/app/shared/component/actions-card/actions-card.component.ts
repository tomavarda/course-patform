import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-actions-card',
  templateUrl: './actions-card.component.html',
  styleUrls: ['./actions-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionsCardComponent {

}
