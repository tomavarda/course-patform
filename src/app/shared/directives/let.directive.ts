import { Directive, Inject, Input, TemplateRef, ViewContainerRef } from '@angular/core';

export class LetContext<T> {
  constructor(private readonly dir: LetDirective<T>) {}

  get appLet(): T {
    return this.dir.appLet;
  }
}

/**
 * Works like *ngIf but does not have a condition
 * Use it to declare the result of pipes calculation
 * (i.e. async pipe)
 */
@Directive({
  selector: '[appLet]',
  standalone: true,
})
export class LetDirective<T> {
  @Input() appLet: T;

  constructor(
    @Inject(ViewContainerRef) viewContainer: ViewContainerRef,
    @Inject(TemplateRef) templateRef: TemplateRef<LetContext<T>>
  ) {
    viewContainer.createEmbeddedView(templateRef, new LetContext<T>(this));
  }
}
