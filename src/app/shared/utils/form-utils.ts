import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

export class FormUtils {
  static patternValidator(regExp: RegExp, error: ValidationErrors): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      // @ts-ignore
      return regExp.test(control.value) ? null : error;
    };
  }

  static matchFields(fieldSelector: string, matchFieldSelector: string) {
    return (formGroup: FormGroup) => {
      const field = formGroup.get(fieldSelector);
      const matchField = formGroup.get(matchFieldSelector);

      return field?.value === matchField?.value ? null : { fieldsNotMatch: true };
    };
  }

  static passwordValidators(): ValidatorFn | null {
    return Validators.compose([
      Validators.required,
      Validators.minLength(6),
      FormUtils.patternValidator(/\d/, { hasNumber: true }),
      FormUtils.patternValidator(/[a-z]/, { hasLowerCase: true }),
      // FormUtils.patternValidator(/[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]+/, { hasSpecialCharacter: true }),
      // FormUtils.patternValidator(/[A-Z]/, { hasUpperCase: true }),
    ]);
  }
}
