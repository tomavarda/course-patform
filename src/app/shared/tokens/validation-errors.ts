import { InjectionToken } from '@angular/core';
import { isNil } from 'lodash';

export type ValidationErrors = Record<string, (...args: any) => string | null>;

export const DEFAULT_VALIDATION_ERRORS: ValidationErrors = {
  email: () => 'Enter valid email address',
  required: () => 'Field is required',
  maxlength: ({ requiredLength, actualLength }) =>
    requiredLength ? `Maximum length is ${requiredLength}, now ${actualLength}` : null,
  minlength: ({ requiredLength, actualLength }) =>
    requiredLength ? `Minimum length is ${requiredLength}, now ${actualLength}` : null,
  hasNumber: () => 'The field must contain at least one number',
  hasUpperCase: () => 'The field must contain at least one character in upper case',
  hasLowerCase: () => 'The field must contain at least one character in upper case',
  hasSpecialCharacter: () => 'The field must contain at least one special character',
  pattern: () => 'Please use the right format',
  fieldsNotMatch: () => 'The fields do not match',
};

export const VALIDATION_ERRORS = new InjectionToken<ValidationErrors>('Validation errors', {
  factory: () => ({
    ...DEFAULT_VALIDATION_ERRORS,
  }),
});
