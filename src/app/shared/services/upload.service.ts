import { Injectable } from '@angular/core';
import { getStorage, ref, uploadBytes, UploadResult } from '@angular/fire/storage';
import { from, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UploadService {
  constructor() {}

  uploadFile(path: string, file?: any): Observable<UploadResult> {
    const storage = getStorage();
    const storageRef = ref(storage, path);
    console.log(storageRef);

    return from(uploadBytes(storageRef, file));
  }
}
